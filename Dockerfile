
##
FROM ubuntu:14.04

# Requirements
RUN apt-get -y update
RUN apt-get -y install wget git build-essential python
RUN apt-get -y install libexpat1-dev libexpat1 libicu-dev
RUN apt-get -y install redis-server
RUN apt-get -y install nodejs npm

# Symlink required on Ubuntu for node
RUN ln -s /usr/bin/nodejs /usr/bin/node

# Install supervisord
RUN apt-get -y install supervisor
RUN mkdir -p /var/log/supervisor

# Supervisor Configuration
add ./supervisord/conf.d/supervisord.conf /etc/supervisor/conf.d/supervisord.conf
ADD ./supervisord/conf.d/redis-server.conf /etc/supervisor/conf.d/redis-server.conf
ADD ./supervisord/conf.d/hubot.conf /etc/supervisor/conf.d/hubot.conf

# Install hubot
RUN mkdir /opt/hubot
WORKDIR /opt/hubot
RUN npm install coffee-script hubot htmlparser -g
RUN hubot --create .
RUN npm install --save hubot-hipchat
RUN chmod 755 bin/hubot 

# Hubot Scripts
ADD ./hubot/hubot-scripts.json /opt/hubot/hubot-scripts.json

CMD supervisord -n

